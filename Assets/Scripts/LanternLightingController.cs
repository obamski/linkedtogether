using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternLightingController : MonoBehaviour
{
    public GameObject LanternLit;
    public GameObject LanternUnlit;

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Enter tr");
        if(other.GetComponent<PlayerController>() == null)
            return;
        LanternLit.SetActive(true);
        LanternUnlit.SetActive(false);
    }
}