using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    public float animDelay;

    public IEnumerator Start()
    {
        yield return new WaitForSeconds(animDelay);
        GetComponent<Animator>().enabled = true;
    }

}
