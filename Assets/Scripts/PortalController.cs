using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalController : MonoBehaviour
{
    public SideState PortalState;
    public Animator portalAnimator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null &&
            other.GetComponent<PlayerController>().playerSide == PortalState)
        {
            portalAnimator.Play("LanternLit");
            var gameManager = FindObjectOfType<GameManager>();
            if (gameManager.onePlayerLeft)
            {
                Debug.Log("yay");
                StartCoroutine(gameManager.VanishLastPlayer());

            }
            else
            {
                gameManager.onePlayerLeft = true;
                StartCoroutine(gameManager.ChangeSide());
            }

        }
    }
}