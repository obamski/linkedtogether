using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


public class PlayerController : MonoBehaviour
{
    public CharacterController characterController;
    public U10PS_DissolveOverTime DissolveOverTime;
    public float playerSpeed;
    private bool levelStarted = false;
    public bool isActive;
    public Animator playerAnimator;
    public GameObject fakeLantern;
    public GameObject realLantern;
   // public Vector3 fakeLanternStartingPosition = new Vector3(0, -2.988f, 0);
    public GameObject fakeLanternPrefab;
    public Material DissolveMaterial;
    public CinemachineVirtualCamera _virtualCamera;
    public SideState playerSide;

    public SkinnedMeshRenderer fakeLanternMesh;
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);
        levelStarted = true;
        transform.GetComponent<BoxCollider>().enabled = true;
       // fakeLantern.GetComponent<Rigidbody>().isKinematic = true;
        fakeLantern.transform.localPosition = new Vector3(0, -2.988f, 0);
        // fakeLanternStartingPosition = fakeLantern.transform.localPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<TileController>() == null || !levelStarted || !isActive) return;
        var tileOn = other.GetComponent<TileController>().isActive;
        if (!tileOn)
            StartCoroutine(FallDown());
        else
        {
            StopAllCoroutines();
        }

        Debug.Log($"tile {tileOn}");
    }


    private IEnumerator FallDown()
    {
        yield return new WaitForSeconds(0.5f);

        playerAnimator.Play("Idle");
        var mats = DissolveOverTime.GetComponent<SkinnedMeshRenderer>().materials;
        mats[0] = DissolveMaterial;
        fakeLantern.GetComponent<CapsuleCollider>().isTrigger = true;
        DissolveOverTime.GetComponent<SkinnedMeshRenderer>().materials = mats;
        _virtualCamera.LookAt = fakeLantern.transform;

        StartCoroutine(DissolveOverTime.Dissolve(1));

        realLantern.SetActive(false);
        fakeLantern.SetActive(true);
        isActive = false;

        yield return new WaitForSeconds(1.5f);
        FindObjectOfType<GameManager>().ResetGameState();

        // for (var i = 0; i < 100; i++)
        // {
        //     transform.Translate(0, Time.deltaTime * -(5 + i), 0);
        //     yield return new WaitForSeconds(Time.deltaTime);
        // }
        //
        // gameObject.SetActive(false);
    }

    public void PlayIdleAnim()
    {
        playerAnimator.Play("Idle");
    }

    private void FixedUpdate()
    {
        if (!isActive)
            return;

        var horizontal = Input.GetAxisRaw("Horizontal");
        var vertical = Input.GetAxisRaw("Vertical");

        var direction = new Vector3(horizontal, 0, vertical).normalized;

        if (direction.magnitude < 0.05f)
        {
            playerAnimator.Play("Idle");
            return;
        }

        playerAnimator.Play("WalkCycle");

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.15F);
        characterController.Move(direction * (playerSpeed * Time.deltaTime));
    }
}