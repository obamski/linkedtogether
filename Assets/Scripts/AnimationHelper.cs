using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHelper : MonoBehaviour
{
   public GameObject CM1;
   public GameObject CM2;
   public GameObject playerA;
   public List<GameObject> toTurnOnAfterFlyBy;
   
   public void StartGameAfterFlyby()
   {
      foreach (var o in toTurnOnAfterFlyBy)
      {
         o.SetActive(true);
      }

      StartCoroutine(StartWithDelay());
   }

   IEnumerator StartWithDelay()
   {
      yield return new WaitForSeconds(1);
      Debug.Log("Starting game after floiboi");
      CM2.SetActive(false);
      CM1.SetActive(true);
      playerA.SetActive(true);
      FindObjectOfType<GameManager>().ResetGameState();
   }
}
