using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;

[Serializable]
public class SideOnObject
{
    public SideState SideOn;
    public Collider Collider;
    public GameObject ActiveObject;
}

[Serializable]
public class AnimationWithAnimator
{
    public Animator Animator;
    public string Animation;
}

[Serializable]
public class PlayerObject
{
    public PlayerController PlayerController;
    public GameObject WholeObject;
    public SkinnedMeshRenderer MeshRenderer;
    public U10PS_DissolveOverTime DissolveController;
    public GameObject RealLantern;
    public GameObject FakeLantern;
}

[Serializable]
public class GameState
{
    public Vector3 PlayerAPos;
    public Vector3 PlayerBPos;
    public List<GameObject> LanternsOff;
    public List<GameObject> LanternsOn;
    
}


public class GameManager : MonoBehaviour
{
    public List<SideOnObject> SideChangeObjects;

    public GameState startingState;
    public bool onePlayerLeft;

    public Material DissolveMaterial;
    public PlayerObject PlayerAObject;
    public PlayerObject PlayerBObject;

    public CinemachineVirtualCamera _virtualCamera;

    public SideState currentSide;
    public List<AnimationWithAnimator> sideAOnAnimations;
    public List<AnimationWithAnimator> sideBOnAnimations;

    public List<GameObject> SideAObjectsOn;

    public List<GameObject> SideBObjectsOn;

    public void ResetGameState()
    {
        PlayerAObject.PlayerController.gameObject.transform.position = startingState.PlayerAPos;
        PlayerBObject.PlayerController.gameObject.transform.position = startingState.PlayerBPos;
        foreach (var o in startingState.LanternsOff)
        {
            o.SetActive(false);
        }
        foreach (var o in startingState.LanternsOn)
        {
            o.SetActive(true);
        }
        PlayerAObject.FakeLantern.transform.localPosition =  new Vector3(0, -2.988f, 0);
        PlayerBObject.FakeLantern.transform.localPosition =  new Vector3(0, -2.988f, 0);

        PlayerAObject.FakeLantern.GetComponent<CapsuleCollider>().isTrigger = false;
        PlayerBObject.FakeLantern.GetComponent<CapsuleCollider>().isTrigger = false;
        StartCoroutine(ChangeToSideA());
    }

    
    private void Start()
    {
        startingState.PlayerAPos = PlayerAObject.PlayerController.gameObject.transform.position;
        startingState.PlayerBPos = PlayerBObject.PlayerController.gameObject.transform.position;
        currentSide = SideState.SideA;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space) && !onePlayerLeft)
        {
            StartCoroutine(ChangeSide());
        }
    }

    public IEnumerator ChangeSide()
    {
        currentSide = currentSide == SideState.SideA ? SideState.SideB : SideState.SideA;

        if (currentSide == SideState.SideA)
        {
            StartCoroutine(ChangeToSideA());
        }

        if (currentSide == SideState.SideB)
        {
            StartCoroutine(ChangeToSideB());
        }


        yield return new WaitForSeconds(0.1f);
    }

    private IEnumerator ChangeToSideA()
    {
        PlayerBObject.PlayerController.PlayIdleAnim();
        
        PlayerBObject.PlayerController.isActive = false;
        PlayerAObject.FakeLantern.GetComponent<Rigidbody>().isKinematic = true;
        PlayerAObject.FakeLantern.transform.localPosition = new Vector3(0, -2.988f, 0);
        PlayerAObject.FakeLantern.transform.localRotation = Quaternion.identity;
        PlayerAObject.FakeLantern.GetComponent<Rigidbody>().isKinematic = false;
            
        PlayerAObject.FakeLantern.SetActive(false);
        PlayerAObject.RealLantern.SetActive(true);
        ///////////
        //
        // foreach (var o in SideAObjectsOn)
        // {
        //     o.SetActive(true);
        // }
        //
        // foreach (var animationWithAnimator in sideAOnAnimations)
        // {
        //     animationWithAnimator.Animator.Play(animationWithAnimator.Animation);
        // }
        //

        foreach (var o in SideChangeObjects.Where(q => q.SideOn == SideState.SideA))
        {
            o.Collider.isTrigger = true;
          //  o.ActiveObject?.SetActive(true);
        }

        foreach (var o in SideChangeObjects.Where(q => q.SideOn == SideState.SideB))
        {
            o.Collider.isTrigger = false;
           // o.ActiveObject?.SetActive(false);
        }


        /////////////
        var mats = PlayerBObject.MeshRenderer.materials;
        mats[0] = DissolveMaterial;

        PlayerBObject.MeshRenderer.materials = mats;

        StartCoroutine(PlayerBObject.DissolveController.Dissolve(1));
        yield return new WaitForSeconds(0.5f);

        PlayerBObject.RealLantern.SetActive(false);
        if(!onePlayerLeft)
            PlayerBObject.FakeLantern.SetActive(true);
        PlayerBObject.WholeObject.SetActive(false);

        PlayerAObject.WholeObject.SetActive(true);
        mats = PlayerAObject.MeshRenderer.materials;
        mats[0] = DissolveMaterial;
        PlayerAObject.MeshRenderer.materials = mats;

        StartCoroutine(PlayerAObject.DissolveController.Condense(1.5f));
        PlayerAObject.PlayerController.isActive = true;

       // yield return new WaitForSeconds(1);
        _virtualCamera.LookAt = PlayerAObject.PlayerController.gameObject.transform;
        _virtualCamera.Follow = PlayerAObject.PlayerController.gameObject.transform;


        yield return new WaitForSeconds(0.1f);
    }

    public IEnumerator VanishLastPlayer()
    {
        PlayerAObject.PlayerController.PlayIdleAnim();

        var mats = PlayerAObject.MeshRenderer.materials;
        mats[0] = DissolveMaterial;

        PlayerAObject.MeshRenderer.materials = mats;

        StartCoroutine(PlayerAObject.DissolveController.Dissolve(1));
        yield return new WaitForSeconds(1.5f);

        PlayerAObject.WholeObject.SetActive(false);
        PlayerAObject.FakeLantern.SetActive(false);
    }

    private IEnumerator ChangeToSideB()
    {
        PlayerAObject.PlayerController.PlayIdleAnim();

        PlayerAObject.PlayerController.isActive = false;

        PlayerBObject.FakeLantern.GetComponent<Rigidbody>().isKinematic = true;
        PlayerBObject.FakeLantern.transform.localPosition = new Vector3(0, -2.988f, 0);
        PlayerBObject.FakeLantern.transform.localRotation = Quaternion.identity;
        PlayerBObject.FakeLantern.GetComponent<Rigidbody>().isKinematic = false;

        PlayerBObject.FakeLantern.SetActive(false);
        PlayerBObject.RealLantern.SetActive(true);


        ///////////////
        // foreach (var o in SideBObjectsOn)
        // {
        //     o.SetActive(true);
        // }
        //
        // foreach (var animationWithAnimator in sideBOnAnimations)
        // {
        //     animationWithAnimator.Animator.Play(animationWithAnimator.Animation);
        // }


        foreach (var o in SideChangeObjects.Where(q => q.SideOn == SideState.SideB))
        {
            o.Collider.isTrigger = true;
           // o.ActiveObject?.SetActive(true);
        }

        foreach (var o in SideChangeObjects.Where(q => q.SideOn == SideState.SideA))
        {
            o.Collider.isTrigger = false;
            // if (o.ActiveObject != null)
            //     o.ActiveObject.SetActive(false);
        }


        var mats = PlayerAObject.MeshRenderer.materials;
        mats[0] = DissolveMaterial;

        PlayerAObject.MeshRenderer.materials = mats;

        StartCoroutine(PlayerAObject.DissolveController.Dissolve(1));
        yield return new WaitForSeconds(0.5f);
        PlayerAObject.RealLantern.SetActive(false);
        if(!onePlayerLeft)
            PlayerAObject.FakeLantern.SetActive(true);
        PlayerAObject.WholeObject.SetActive(false);

        PlayerBObject.WholeObject.SetActive(true);
        mats = PlayerBObject.MeshRenderer.materials;
        mats[0] = DissolveMaterial;
        PlayerBObject.MeshRenderer.materials = mats;

        StartCoroutine(PlayerBObject.DissolveController.Condense(1.5f));
        PlayerBObject.PlayerController.isActive = true;

      //  yield return new WaitForSeconds(1);
        _virtualCamera.LookAt = PlayerBObject.PlayerController.gameObject.transform;
        _virtualCamera.Follow = PlayerBObject.PlayerController.gameObject.transform;

        yield return new WaitForSeconds(0.1f);
    }
}

public enum SideState
{
    SideA,
    SideB
}