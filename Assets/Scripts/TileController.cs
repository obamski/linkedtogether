using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TileController : MonoBehaviour
{
    public GameObject Tile;
    private Vector3 _tileStartingPos;
    public bool isActive;

    public void Awake()
    {
        Tile = gameObject.transform.GetChild(0).gameObject;
        _tileStartingPos = Tile.gameObject.transform.localPosition;
        Tile.gameObject.transform.localPosition = new Vector3(_tileStartingPos.x, -40, _tileStartingPos.z);
        Tile.SetActive(false);
       //isActive = true;
    }

    private bool interrupted;

    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<LanternController>() == null)
            return;
        StopAllCoroutines();
        StartCoroutine(MoveTileUp());
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<LanternController>() == null)
            return;
        
        StopAllCoroutines();
        StartCoroutine(MoveTileDown());
    }

    private IEnumerator MoveTileUp()
    {
        // yield return new WaitForSeconds(Random.Range(0.2f, 03f));
        isActive = true;
        yield return new WaitForSeconds(Random.Range(0.05f, 0.2f));

        var goalPosition = new Vector3(_tileStartingPos.x, _tileStartingPos.y + Random.Range(-0.2f, 0.2f),
            _tileStartingPos.z);
        Tile.SetActive(true);
        var counter = 0;
        while (Tile.gameObject.transform.localPosition != goalPosition)
        {
            counter++;
            yield return new WaitForSeconds(Time.deltaTime);
            var delta = Math.Abs(goalPosition.y - Tile.transform.localPosition.y) * Time.deltaTime * 10;
            Tile.transform.localPosition = Vector3.MoveTowards(Tile.transform.localPosition, goalPosition, delta);
        }
    }

    private IEnumerator MoveTileDown()
    {
        isActive = false;
        yield return new WaitForSeconds(Random.Range(0.05f, 0.1f));
        var goalPosition = new Vector3(_tileStartingPos.x, -50, _tileStartingPos.z);
        var delta = Random.Range(0.4f, 2f);
        for (var i = 0; i < 15; i++)
        {
            Tile.transform.Translate(0, 0, Time.deltaTime * -(50 + i));
            yield return new WaitForSeconds(Time.deltaTime);
        }

        Tile.SetActive(false);
    }
}