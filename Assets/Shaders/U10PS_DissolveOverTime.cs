﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
//
// [RequireComponent(typeof(MeshRenderer))]
// public class U10PS_DissolveOverTime : MonoBehaviour
// {
//     private MeshRenderer meshRenderer;
//
//     public float speed = .5f;
//
//     private void Start(){
//         meshRenderer = this.GetComponent<MeshRenderer>();
//     }
//
//     private float t = 0.0f;
//     private void Update(){
//         Material[] mats = meshRenderer.materials;
//
//         mats[0].SetFloat("_Cutoff", Mathf.Sin(t * speed));
//         t += Time.deltaTime;
//         
//         // Unity does not allow meshRenderer.materials[0]...
//         meshRenderer.materials = mats;
//     }
// }


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(MeshRenderer))]
public class U10PS_DissolveOverTime : MonoBehaviour
{
    private Renderer meshRenderer;

    private Material[] mats;
    public bool dissolveAtStart;
    public bool condenseAtStart;
    public float dissolveAfter;
    public float speed;

    void OnEnable()
    {
        if (dissolveAtStart)
            StartCoroutine(Dissolve(speed));
        else if (condenseAtStart)
            StartCoroutine(Condense(speed));
    }

    public void DissolveInvisible(float speed)
    {
        StartCoroutine(Dissolve(speed));
    }

    public void DissolveVisible(float speed)
    {
        StartCoroutine(Condense(speed));
    }

    public IEnumerator Dissolve(float speed)
    {
        if (GetComponent<SkinnedMeshRenderer>() != null)
            meshRenderer = GetComponent<SkinnedMeshRenderer>();
        else
            meshRenderer = this.GetComponent<MeshRenderer>();

        mats = meshRenderer.materials;
        mats[0].SetFloat("_Cutoff", 0.279f);
        var t = 0.279f;

        while (mats[0].GetFloat("_Cutoff") < 1)
        {
            Material[] mats = meshRenderer.materials;
            mats[0].SetFloat("_Cutoff", t * speed);
            t += Time.deltaTime;

            // Unity does not allow meshRenderer.materials[0]...
            meshRenderer.materials = mats;
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    public Material materialAfterCondense;

    public IEnumerator Condense(float speed)
    {
        // Debug.Log("Condensin");
        // mats = meshRenderer.materials;
        // mats[0] = materialAfterCondense;
        // meshRenderer.materials = mats;
        // yield return new WaitForSeconds(Time.deltaTime);
        //
        // yield break;
        if (GetComponent<SkinnedMeshRenderer>() != null)
            meshRenderer = GetComponent<SkinnedMeshRenderer>();
        else
            meshRenderer = this.GetComponent<MeshRenderer>();

        mats = meshRenderer.materials;
        mats[0].SetFloat("_Cutoff", 1);
        
        var t = 1f;
        while (mats[0].GetFloat("_Cutoff") > 0.4f)
        {
            Material[] mats = meshRenderer.materials;
            mats[0].SetFloat("_Cutoff", t * speed);
            t -= Time.deltaTime;

            // Unity does not allow meshRenderer.materials[0]...
            meshRenderer.materials = mats;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        mats = meshRenderer.materials;
        mats[0] = materialAfterCondense;
        meshRenderer.materials = mats;
    }
}